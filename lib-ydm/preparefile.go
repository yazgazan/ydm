
package ydm

func (i *Item) PrepareFile() error {
  var err error
  var size int64

  i.Safe(func () {
    if i.restoring == false {
      err = i.GetFileName()
    }
  })
  if err != nil {
    i.ReportError(err)
    return err
  }

  err = i.openPartFile()
  if err != nil {
    return err
  }
  i.Safe(func () {
    size = i.Size
  })
  if size != -1 {
    err = i.initFile()
    if err != nil {
      i.ReportError(err)
      return err
    }
  }
  return nil
}

