
package ydm

func (i *Item) initFile() error {
  var n int
  var err error
  var c int64 = 0
  var size int64
  var restoring bool

  i.Safe(func () {
    size = i.Size
    restoring = i.restoring
  })
  if restoring == true {
    return nil
  }
  for c < size {
    var blocksize int64 = Buffers_Size

    if (c + blocksize) > size {
      blocksize = size - c
    }
    buf := make([]byte, blocksize)
    i.Safe(func () {
      n, err = i.partfile.Write(buf)
    })
    if err != nil {
      i.ReportError(err)
      return err
    }
    c += blocksize
  }
  return nil
}

