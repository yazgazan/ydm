
package main

import (
  "github.com/yazgazan/ydm/lib-ydm"
)

type ListItem struct {
  Id            int
  Threads       int
  Url           string
  Dir           string
  State         string
  Error         string
  Filename      string
  Partfilename  string
}

func List(cmd *Command, conf *Config, b *Bank) (*Response, error) {
  var list = make([]ListItem, len(b.Items))
  var j = 0

  for i := range b.Items {
    var errStr string = ""
    b.Items[i].Data.Safe(func () {
      if b.Items[i].Data.Error != nil {
        errStr = b.Items[i].Data.Error.Error()
      }
      list[j] = ListItem{
        Id: b.Items[i].Id,
        Threads: b.Items[i].Data.NThreads,
        Url: b.Items[i].Data.Url,
        Dir: b.Items[i].Data.Dir,
        State: ydm.StateToString(b.Items[i].Data.State),
        Error: errStr,
        Filename: b.Items[i].Data.Filename,
        Partfilename: b.Items[i].Data.Partfilename,
      }
    })
    j++
  }
  return &Response{
    Code: OkListing,
    String: "listing",
    Data: list,
  }, nil
}

