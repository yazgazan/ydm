
package ydm

import (
  "encoding/json"
)

func (i *Item) JsonState() ([]byte, error) {
  return json.Marshal(i)
}

