
package ydm

import (
  "net/http"
)

func (i *Item) Head() (*http.Response, error) {
  var url string
  client := &http.Client{}

  i.Safe(func () {
    url = i.Url
  })
  req, err := http.NewRequest("HEAD", url, nil)
  if err != nil {
    return nil, err
  }

  i.Safe(func () {
    if i.Header != nil {
      req.Header = copyHeaders(i.Header)
    }
  })

  resp, err := client.Do(req)
  if err != nil {
    return nil, err
  }
  return resp, nil
}

