
package ydm

func (i *Item) GetState() int {
  var state int

  i.Safe(func () {
    state = i.State
  })

  return state
}

func StateToString(state int) string {
  switch state {
  case State_Stopped:
    return "stopped"
  case State_Started:
    return "started"
  case State_Running:
    return "downloading"
  case State_Renaming:
    return "renaming"
  case State_Finished:
    return "completed"
  case State_Error:
    return "error"
  default:
    return "unknown state"
  }
  return "unknown state"
}

