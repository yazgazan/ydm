
package ydm

import (
  "fmt"
)

func (i *Item) UpdateState() {
  stopped := 0
  running := 0
  finished := 0
  err := 0
  move := false

  i.Safe(func () {
    if i.State == State_Finished || i.State == State_Renaming {
      return
    }

    for id := range i.Threads {
      switch i.Threads[id].State {
      case State_Stopped:
        stopped++
      case State_Started:
        running++
      case State_Running:
        running++
      case State_Finished:
        finished++
      case State_Error:
        err++
      default:
        err++
      }
    }
    if finished == i.NThreads {
      i.State = State_Finished
      if i.Automove == true {
        move = true
      }
    } else if running != 0 {
      i.State = State_Running
    } else if stopped != 0 {
      i.State = State_Stopped
    } else if err != 0 {
      fmt.Println("Error ...")
      i.State = State_Error
    }
  })
  if move == true {
    err := i.MoveFile()
    if err != nil {
      i.ReportError(err)
      return
    }
  }
}

