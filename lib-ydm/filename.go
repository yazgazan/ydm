
package ydm

import (
  "fmt"
  "path"
  "net/url"
)

func (i *Item) GetFileNameFromUrl() (string, error) {
  parsedUrl, err := url.Parse(i.Url)

  if err != nil {
    i.Error = err
    return "", err
  }
  _, file := path.Split(parsedUrl.Path)
  if len(file) == 0 {
    return "index", nil
  }
  return file, nil
}

func (i *Item) GetFileName() error {
  var filename string
  var partfilename string
  var err error

  if len(i.Filename) == 0 {
    filename, err = i.GetFileNameFromUrl()
    if err != nil {
      return err
    }
  } else {
    filename = i.Filename
  }

  if len(i.Partfilename) == 0 {
    partfilename = fmt.Sprintf("%s.part", filename)
  } else {
    partfilename = i.Partfilename
  }

  if (checkFileExists(filename) == false) &&
  (checkFileExists(partfilename) == false) {
    i.Filename = filename
    i.Partfilename = partfilename
    return nil
  }
  for count := 0; count < count + 1; count++ {
    var newpartfilename string

    newfilename := fmt.Sprintf("%s.%d", filename, count)
    if len(i.Partfilename) == 0 {
      newpartfilename = fmt.Sprintf("%s.%d.part", filename, count)
    } else {
      newpartfilename = fmt.Sprintf("%s.%d", i.Partfilename, count)
    }

    if (checkFileExists(newfilename) == false) &&
    (checkFileExists(newpartfilename) == false) {
      i.Filename = newfilename
      i.Partfilename = newpartfilename
      return nil
    }
  }
  return fmt.Errorf("file already exists")
}

