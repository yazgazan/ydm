
package ydm

func copyHeaders(headers map[string][]string) map[string][]string {
  ret := make(map[string][]string)

  for name := range headers {
    ret[name] = make([]string, len(headers[name]))
    for id := range headers[name] {
      ret[name][id] = headers[name][id]
    }
  }
  return ret
}

