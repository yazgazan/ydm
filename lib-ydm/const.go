
package ydm

const (
  State_Stopped   = iota
  State_Started   = iota
  State_Running   = iota
  State_Renaming  = iota
  State_Finished  = iota
  State_Error     = iota
)

const (
  Buffers_Size = 4096
)

