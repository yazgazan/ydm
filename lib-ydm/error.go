
package ydm

func (i *Item) ReportError(err error) {
  i.Safe(func () {
    i.State = State_Error
    i.Error = err
  })
}

