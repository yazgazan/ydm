
package main

import (
  "log"

  "github.com/yazgazan/ydm/lib-ydm"
)

type AddedItemResp struct {
  Id      int
  Threads int
  Dir     string
}

func AddItem(cmd *Command, conf *Config, b *Bank) (*Response, error) {
  if len(cmd.Url) == 0 {
    return &Response{
      Code: ErrRequest,
      String: "parameter url is required",
      Data: nil,
    }, nil
  }
  i := b.Add(&Item{
    Id: -1,
    Data:  ydm.NewItem(ydm.Item{
      Url: cmd.Url,
      Dir: cmd.Dir,
      NThreads: cmd.Threads,
    }),
  })
  err := i.InitStateFile(conf)
  if err != nil {
    log.Printf("Couldn't write state file : %s\n", err)
    return &Response{
      Code: ErrWriteState,
      String: "item added, athough state file couldn't be written (see logs for further details)",
      Data: AddedItemResp{
        Id: i.Id,
        Threads: i.Data.NThreads,
        Dir: i.Data.Dir,
      },
    }, nil;
  }
  return &Response{
    Code: OkAdded,
    String: "item added successfully",
    Data: AddedItemResp{
      Id: i.Id,
      Threads: i.Data.NThreads,
      Dir: i.Data.Dir,
    },
  }, nil;
}

