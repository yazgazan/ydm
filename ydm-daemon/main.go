
package main

import (
  "log"
)

func main() {
  conf := InitConfig()
  bank := InitBank(conf)
  if err := StartHttp(conf, &bank); err != nil {
    log.Fatalf("Error starting http : %s\n", err)
  }
}

