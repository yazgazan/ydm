
package ydm

type Thread struct {
  Id        int
  Begin     int64
  End       int64
  Progress  int64
  State     int
  stop      chan bool
  running   bool
}

func (t *Thread) Init(id int) {
  t.Id = id
  t.State = State_Stopped
  t.stop = make(chan bool)
  t.running = false
}

func (t *Thread) InitRestore() {
  t.stop = make(chan bool)
  t.running = false
}

func (t *Thread) isStopped() bool {
  select {
  case stopped := <-t.stop:
    return stopped
  default:
    return false
  }
  return false
}

func (t *Thread) Stop() {
  t.stop <- true
}

