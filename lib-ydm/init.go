
package ydm

func (i *Item) Init() {
  i.State = State_Stopped
  i.Error = nil
  i.file = nil
  i.lock = make(chan bool, 1)
  i.Automove = true
  if i.NThreads == 0 {
    i.NThreads = 1
  }
  if i.restoring == false {
    i.Threads = make([]Thread, i.NThreads)
  }
  for id := range i.Threads {
    if i.restoring == false {
      i.Threads[id].Init(id)
    } else {
      i.Threads[id].InitRestore()
      i.Threads[id].running = false
    }
  }
}

func NewItem(i Item) Item {
  i.restoring = false
  i.Init()
  return i
}

