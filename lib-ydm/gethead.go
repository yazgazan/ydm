
package ydm

import (
  "fmt"
)

func (i *Item) GetHead() error {
  var url string

  i.Safe(func () {
    url = i.Url
  })

  resp, err := i.Head()
  if err != nil {
    i.ReportError(err)
    return err
  }

  if resp.StatusCode != 200 {
    err = fmt.Errorf("status %d != 200", resp.StatusCode)
    i.ReportError(err)
    return err
  }

  i.Safe(func () {
    i.Size = resp.ContentLength
    if (i.Size == -1) && (i.NThreads != 1) {
      i.NThreads = 1
      i.Threads = make([]Thread, 1)
    }

    i.SetupThreadsBoundaries()
  })
  return nil
}

func (i *Item) SetupThreadsBoundaries() {
  for j := range i.Threads {
    i.Threads[j].Begin = int64(j) * (i.Size / int64(i.NThreads))
    if j == i.NThreads - 1 {
      i.Threads[j].End = i.Size
    } else {
      i.Threads[j].End = i.Threads[j].Begin
      i.Threads[j].End += i.Size / int64(i.NThreads)
    }
    i.Threads[j].Progress = i.Threads[j].Begin
  }
}

