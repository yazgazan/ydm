
package main

import (
  "fmt"

  "github.com/yazgazan/ydm/lib-ydm"
)

func StartItem(cmd *Command, conf *Config, b *Bank) (*Response, error) {
  if cmd.Id == -1 {
    return &Response{
      Code: ErrRequest,
      String: "parameter id is required",
      Data: nil,
    }, nil
  }

  i := b.Get(cmd.Id)
  if i == nil {
    return &Response{
      Code: ErrNoSuchId,
      String: fmt.Sprintf("Couldn't find item #%d", cmd.Id),
      Data: nil,
    }, nil
  }

  state := i.Data.GetState()

  if state == ydm.State_Stopped || state == ydm.State_Error {
    i.Data.Run()
    return &Response{
      Code: OkStarted,
      String: "item started successfuly",
      Data: nil,
    }, nil
  }
  if state == ydm.State_Finished {
    return &Response{
      Code: ErrFinished,
      String: "item has already completed",
      Data: nil,
    }, nil;
  }
  return &Response{
    Code: ErrRunning,
    String: "item is already running",
    Data: nil,
  }, nil
}

