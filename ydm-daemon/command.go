
package main

import (
  "fmt"
  "strconv"
  "net/http"
)

type Command struct {
  Url       string // for add only
  Dir       string // for add only
  Id        int    // for any command but add
  Threads   int // for add, start, restart and update
}

func (c *Command) Init(conf *Config) {
  c.Url = ""
  c.Dir = conf.Dir
  c.Id = -1
  c.Threads = conf.Threads
}

func (c *Command) Load(conf *Config, r *http.Request) error {
  var err error

  c.Init(conf)

  urls, ok := r.Form["url"]
  if ok == true {
    if len(urls) == 0 {
      return fmt.Errorf("Unexpected error: len(urls) was 0 ...")
    }
    if len(urls) > 1 {
      return fmt.Errorf("Only one url is allow in a request")
    }
    c.Url = urls[0]
  }

  dirs, ok := r.Form["dir"]
  if ok == true {
    if len(dirs) == 0 {
      return fmt.Errorf("Unexpected error: len(dirs) was 0 ...")
    }
    if len(dirs) > 1 {
      return fmt.Errorf("Only one directory is allow in a request")
    }
    c.Dir = dirs[0]
  }

  ids, ok := r.Form["id"]
  if ok == true {
    if len(ids) == 0 {
      return fmt.Errorf("Unexpected error: len(ids) was 0 ...")
    }
    if len(ids) > 1 {
      return fmt.Errorf("Only one id is allow in a request")
    }
    c.Id, err = strconv.Atoi(ids[0])
    if err != nil {
      return err
    }
  }

  threads, ok := r.Form["threads"]
  if ok == true {
    if len(threads) == 0 {
      return fmt.Errorf("Unexpected error: len(threads) was 0 ...")
    }
    if len(threads) > 1 {
      return fmt.Errorf("Only one Threads value is allow in a request")
    }
    c.Threads, err = strconv.Atoi(threads[0])
    if err != nil {
      return err
    }
  }
  return nil
}

/*
 * Commands Types :
 * 
 * 1 - add : required : Url; optionals : Dir, Threads
 * 1 - start : required : Id
 * 2 - infos : required : Id
 * 2 - list : required : none
 * 3 - rm : required : Id
 * 3 - restart : required : Id
 * 3 - stop : required : Id
 * 4 - update : required : Id; optionals : Url, Dir
 * 5 - request_push : required : Url (push url);
 */

