
package ydm

func (i *Item) Lock() {
  i.lock <- true
}

func (i *Item) Release() {
  <-i.lock
}

func (i *Item) Safe(cb func()) {
  i.Lock()
  cb()
  i.Release()
}

