
package main

import (
  "log"
  "fmt"
  "encoding/json"

  "net/http"
)

const (
  OkNoErr = 0
  OkAdded = 1
  OkStarted = 2
  OkListing = 3
  OkInfo = 4
  ErrUnknownError = 1000
  ErrParseForm = 1001
  ErrNoResponse = 1002
  ErrFailJson = 1003
  ErrForm = 1004
  ErrRequest = 1005
  ErrNoSuchId = 1006
  ErrFinished = 1007
  ErrRunning = 1008
  ErrWriteState = 1009
)

type Response struct {
  Code    int
  String  string
  Data    interface{}
}

type cbWrapper func (*Command, *Config, *Bank) (*Response, error)

func Wrapper(cb cbWrapper, conf *Config, bank *Bank) http.HandlerFunc {
  return func (w http.ResponseWriter, r *http.Request) {
    var ret *Response
    var cmd Command

    err := r.ParseForm()
    if err != nil {
      errjson, _ := json.Marshal(Response{
        Code: ErrParseForm,
        String: "Couldn't parse form, refer to the logs",
        Data: nil,
      })
      log.Printf("Error occured parsing form : %s\n", err)
      http.Error(w, string(errjson), 500)
      return
    }

    err = cmd.Load(conf, r)
    if err != nil {
      errjson, _ := json.Marshal(Response{
        Code: ErrForm,
        String: "Malformated request, see Data for details.",
        Data: err,
      })
      log.Printf("Error occured parsing form, returning 500 : %s\n", err)
      http.Error(w, string(errjson), 500)
      return
    }
    fmt.Printf("%+v\n", cmd)

    ret, err = cb(&cmd, conf, bank)

    if err != nil {
      errjson, _ := json.Marshal(Response{
        Code: -1,
        String: "Unknown error",
        Data: nil,
      })
      log.Printf("Error occured, returning 500 : %s\n", err)
      http.Error(w, string(errjson), 500)
      return
    }
    if ret == nil {
      errjson, _ := json.Marshal(Response{
        Code: ErrNoResponse,
        String: "Don't have any response to send",
        Data: nil,
      })
      log.Printf("No response to send back, returning 500\n")
      http.Error(w, string(errjson), 500)
      return
    }

    retjson, err := json.Marshal(ret)
    if err != nil {
      errjson, _ := json.Marshal(Response{
        Code: ErrFailJson,
        String: "Couldn't Marshal the response",
        Data: nil,
      })
      log.Printf("Error Marshalling Response, returing 500 : %s\n", err)
      http.Error(w, string(errjson), 500)
      return
    }
    fmt.Fprint(w, string(retjson))
  }
}

