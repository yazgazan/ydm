
package ydm

func (i *Item) GetProgress() int64 {
  var total int64 = 0

  if i.Size == -1 {
    return 0
  }

  for id := range i.Threads {
    total += i.Threads[id].Progress - i.Threads[id].Begin
  }
  return total
}

