
package ydm

import (
  "os"
  "net/http"
)

type Item struct {
  Url           string
  Dir           string
  State         int
  Error         error
  Filename      string
  Partfilename  string
  Progress      int
  Size          int64
  ProgressSize  int64
  Threads       []Thread
  NThreads      int
  Header        http.Header
  Automove      bool
  file          *os.File
  partfile      *os.File
  lock          chan bool
  restoring     bool
}

func (i *Item) StopThread(id int) {
  i.Threads[id].Stop()
}

