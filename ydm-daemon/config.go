
package main

import (
  "flag"
)

type Config struct {
  Listen  string
  Dir     string
  BankDir string
  Threads int
}

func InitConfig() (*Config) {
  conf := &Config{}

  flag.StringVar(&conf.Listen, "http", "localhost:5688", "http port to listen to")
  flag.StringVar(&conf.Dir, "dir", "/tmp", "Default download directory")
  flag.StringVar(&conf.BankDir, "bank", "/etc/ydm", "Bank directory, defaulted in /etc/ydm")
  flag.IntVar(&conf.Threads, "threads", 4, "Default number of threads")

  flag.Parse()

  return conf
}

