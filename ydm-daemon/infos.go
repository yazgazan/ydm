
package main

import (
  "fmt"
  // "encoding/json"
)

func Infos(cmd *Command, conf *Config, b *Bank) (*Response, error) {
  // var ret []byte
  // var err error

  if cmd.Id == -1 {
    return &Response{
      Code: ErrRequest,
      String: "parameter id is required",
      Data: nil,
    }, nil
  }

  i := b.Get(cmd.Id)
  if i == nil {
    return &Response{
      Code: ErrNoSuchId,
      String: fmt.Sprintf("Couldn't find item #%d", cmd.Id),
      Data: nil,
    }, nil
  }

//   i.Data.Safe(func () {
//     ret, err = json.Marshal(i)
//   })
//   if err != nil {
//     return &Response{
//       Code: ErrFailJson,
//       String: "Couldn't create json, see logs for details",
//       Data: nil,
//     }, nil
//   }

  return &Response{
    Code: OkInfo,
    String: "infos",
    Data: i,
  }, nil;
}

