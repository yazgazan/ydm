
package ydm

import (
  "os"
  "fmt"
)

func (i *Item) MoveFile() error {
  var filename string
  var partfilename string

  i.Safe(func () {
    filename = i.Filename
    partfilename = i.Partfilename
  })
  if checkFileExists(filename) == true {
    err := fmt.Errorf("Can't rename file, file already exists")
    i.ReportError(err)
    return err
  }

  err := os.Rename(partfilename, filename)
  if err != nil {
    i.ReportError(err)
    return err
  }
  return nil
}

