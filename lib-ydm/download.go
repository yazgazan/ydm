
package ydm

import (
  "fmt"

  "net/http"
)

func (i *Item) Download(threadId int) {
  var size int64
  var url string
  var start int64
  var end int64
  var threadrunning bool
  client := &http.Client{}

  i.Safe(func () {
    url = i.Url
    start = i.Threads[threadId].Progress
    end = i.Threads[threadId].End
    size = i.Size
    threadrunning = i.Threads[threadId].running
    if threadrunning == false {
      i.Threads[threadId].State = State_Started
    }
  })

  if threadrunning == true {
    return
  }
  i.Safe(func () {
    i.Threads[threadId].running = true
  })
  i.UpdateState()
  req, err := http.NewRequest("GET", url, nil)
  if err != nil {
    i.ReportError(err)
    return
  }

  i.Safe(func () {
    if i.Header != nil {
      req.Header = copyHeaders(i.Header)
    }
  })
  req.Header.Add("Range", fmt.Sprintf("bytes=%d-%d", start, end))
  resp, err := client.Do(req)
  if err != nil {
    i.ReportError(err)
    return
  }

  i.Safe(func () {
    i.Threads[threadId].State = State_Running
  })
  if resp.StatusCode != 200 && size == -1 {
    err = fmt.Errorf("status != 200: %d", resp.StatusCode)
    i.ReportError(err)
    return
  } else if resp.StatusCode != 206 && resp.StatusCode != 200 && size != -1 {
    err = fmt.Errorf("status != 206: %d", resp.StatusCode)
    i.ReportError(err)
    return
  }

  err = i.writeFile(resp, threadId)
  if err != nil {
    i.ReportError(err)
    return
  }
  i.Safe(func () {
    if i.Threads[threadId].State != State_Stopped {
      i.Threads[threadId].State = State_Finished
    }
  })
  i.UpdateState()
  return
}

