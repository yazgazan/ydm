
package main

import (
  "fmt"
  "path"
  "io/ioutil"

  "github.com/yazgazan/ydm/lib-ydm"
)

type Item struct {
  Id        int
  StateFile string
  Data      ydm.Item
}

type Bank struct {
  Dir     string
  Items   map[int]*Item
  LastId  int
}

func InitBank(conf *Config) Bank {
  ret := NewBank()
  ret.Init(conf)

  return ret
}

func NewBank() Bank {
  return Bank{
    Dir: "",
    Items: make(map[int]*Item),
    LastId: -1,
  }
}

func (b *Bank) Init(conf *Config) {
  b.Dir = conf.BankDir
}

func (b *Bank) Add(i *Item) *Item {
  if i.Id == -1 {
    b.LastId++
    i.Id = b.LastId
    b.Items[b.LastId] = i
  } else {
    b.Items[i.Id] = i
    if i.Id > b.LastId {
      b.LastId = i.Id
    }
  }
  return i
}

func (b *Bank) Get(id int) *Item {
  i, ok := b.Items[id]

  if ok == false {
    return nil
  }
  return i
}

func (i *Item) InitStateFile(conf *Config) error {
  var json []byte
  var err error

  i.StateFile = path.Join(conf.BankDir, fmt.Sprintf("%d.json", i.Id))

  i.Data.Safe(func () {
    json, err = i.Data.JsonState()
  })
  if err != nil {
    return err
  }

  err = ioutil.WriteFile(i.StateFile, json, 0666)
  return err
}

