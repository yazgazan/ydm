
package ydm

import (
  "io/ioutil"
  "encoding/json"
)

func Restore(filename string) (Item, error) {
  var i Item

  jsondata, err := ioutil.ReadFile(filename)
  if err != nil {
    return i, err
  }

  err = json.Unmarshal(jsondata, &i)
  if err != nil {
    return i, err
  }
  i.restoring = true
  i.Init()
  return i, nil
}

