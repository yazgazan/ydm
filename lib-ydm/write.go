
package ydm

import (
  "io"
  "net/http"
)

func (i *Item) writeFile(resp *http.Response, threadId int) error {
  var size int64
  var end int64
  var c int64

  defer resp.Body.Close()
  i.Safe(func () {
    size = i.Size
    end = i.Threads[threadId].End
    c = i.Threads[threadId].Progress
  })
  for {
    if i.Threads[threadId].isStopped() == true {
      i.Safe(func () {
        i.Threads[threadId].running = false
        i.Threads[threadId].State = State_Stopped
      })
      break
    }
    bufsize := Buffers_Size
    if c + int64(bufsize) > end {
      bufsize = int(end - c)
    }
    buf := make([]byte, bufsize)
    n, err := resp.Body.Read(buf)
    if n > 0 {
      var err2 error
      i.Safe(func () {
        if size == -1 {
          _, err2 = i.partfile.Write(buf[:n])
        } else {
          _, err2 = i.partfile.WriteAt(buf[:n], c)
        }
      })
      if err2 != nil {
        i.ReportError(err2)
        return err2
      }
      i.Safe(func () {
        c += int64(n)
        i.Threads[threadId].Progress = c
      })
    } else if err == nil {
      return nil // download finished
    }
    if err != nil {
      if err == io.EOF {
        return nil // download finished
      }
      i.ReportError(err)
      return err
    }
  }
  return nil
}

