
package ydm

import (
  "os"
)

func checkFileExists(name string) bool {
  _, err := os.Stat(name)

  if err == nil {
    return true
  }
  return false
}

func (i *Item) openPartFile() error {
  var err error

  i.Safe(func () {
    if i.restoring == false {
      i.partfile, err = os.Create(i.Partfilename)
    } else {
      i.partfile, err = os.OpenFile(i.Partfilename, os.O_RDWR, 0666)
    }
  })
  if err != nil {
    i.ReportError(err)
    return err
  }
  return nil
}

