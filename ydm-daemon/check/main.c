
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(void)
{
  int file;
  char buf[4096] = {'\0'};
  char patern[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  ssize_t c = 0;
  ssize_t n = 0;
  ssize_t i;
  ssize_t offset = 0;

  file = open("test", O_RDONLY);

  if (file == -1)
  {
    file = open("test.part", O_RDONLY);
    if (file == -1)
    {
      return 1;
    }
  }
  while ((n = read(file, buf, 4096)) != 0)
  {
    for (i = 0; i < n; ++i)
    {
      if (patern[c] != buf[i])
      {
        printf("file corrupted at %ld.\n", offset);
        return 2;
      }
      c++;
      if (patern[c] == '\0')
      {
        c = 0;
      }
      offset++;
    }
  }
  printf("file ok.\n");
  return 0;
}

