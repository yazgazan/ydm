
package main

import (
  "fmt"

  "net/http"
)

func StartHttp(conf *Config, bank *Bank) error {
  http.HandleFunc("/add", Wrapper(AddItem, conf, bank))
  http.HandleFunc("/start", Wrapper(StartItem, conf, bank))
  http.HandleFunc("/infos", Wrapper(Infos, conf, bank))
  http.HandleFunc("/list", Wrapper(List, conf, bank))
  http.HandleFunc("/remove", Wrapper(RemoveItem, conf, bank))
  http.HandleFunc("/restart", Wrapper(RestartItem, conf, bank))
  http.HandleFunc("/stop", Wrapper(StopItem, conf, bank))
  http.HandleFunc("/update", Wrapper(UpdateItem, conf, bank))
  http.HandleFunc("/request_push", Wrapper(RequestPush, conf, bank))

  fmt.Printf("Listening on %s\n", conf.Listen)
  err := http.ListenAndServe(conf.Listen, nil);
  return err;
}

