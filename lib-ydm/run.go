
package ydm

func (i *Item) Run() {
  var restoring bool

  i.Safe(func () {
    i.State = State_Started
    restoring = i.restoring
  })

  if restoring == false {
    err := i.GetHead()
    if err != nil {
      return
    }
  }

  err := i.PrepareFile()
  if err != nil {
    return
  }

  i.Safe(func () {
    i.State = State_Running
  })

  for id := range i.Threads {
    var state int

    i.Safe(func () {
      state = i.Threads[id].State
    })
    if state != State_Finished {
      go i.Download(id)
    }
  }
  return
}

